# Portfolio Site

My personal portfolio site using Hexo

## Installation

Use the package manager [npm](https://www.npmjs.com/package/npm) to install dependencies.

```bash
npm install
```

## Usage

To start local server on localhost:4000,

```
hexo server
```

but more effectively run this instead:

```
nodemon -e njk,scss --ignore public/ -L --exec 'hexo server'
```

To watch file updates and auto generate new static files,

```
hexo generate --watch
```
or more effectively

```
nodemon -e html,md,css --ignore public/ -L --exec 'hexo generate'
```
The Nodemon will start watching file updates and automatically runs
Start those two commands, refresh your browser and you should see the new updates.

## More to come...