$(function() {
    console.log('page loaded');
});
$body = $('body');

// Header
$('#menu-toggle').on('click', function() {
    var toggleState = $(this).hasClass('--active');
    if(toggleState){
        console.log('active so lets deactivate it');
        $(this).removeClass('--active');
        $('.header__nav-menu').removeClass('--active');
        $body.removeClass('--menu-active');
    }else{
        console.log('not active so lets make it active');
        $(this).addClass('--active');
        $('.header__nav-menu').addClass('--active');
        $body.addClass('--menu-active');
    }
    
});

// Pop-Up
$('#contact-link').on('click', function() {
    $('#contactPopup').addClass('--active');
    $body.addClass('--popup-active');
});

$('#contactPopup .popup__close').on('click', function() {
    $('#contactPopup').removeClass('--active');
    $body.removeClass('--popup-active');
});